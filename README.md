# TP note Csharp

TP qui a pour but de valider les acquis en conception objet, versionning,UML, T.U, .NET, C# et Winforms


## Objectif ?

réalisation d'un compteur basique sous la forme d'une application Winforms

## Composition ?

- Une bibliothèque de classes C# 
- Une application Winforms en VB.NET 
- Les tests unitaires associés

Le diagramme UML correspondants est fourni sur la feuille d'examen.

## Auteur

> [Aurélie PLET]
