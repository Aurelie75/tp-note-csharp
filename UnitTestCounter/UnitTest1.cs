﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CounterLib;

namespace UnitTestCounter
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestConstructeur()
        {
            Counter Compteur1 = new Counter("Compteur1", 5);

            Assert.AreEqual("Compteur1", Compteur1.GetName());
            Assert.AreEqual(5, Compteur1.GetNum());
        }
        public void TestIncrementer()
        {
            Counter Compteur2 = new Counter("Compteur2", 12);
            Compteur2.incrementer();
            Assert.AreEqual("Compteur2", Compteur2.GetName());
            Assert.AreEqual(13, Compteur2.GetNum());
        }

        public void TestDecrementer()
        {
            Counter Compteur3 = new Counter("Compteur3", 12);
            Compteur3.decrementer();
            Assert.AreEqual("Compteur1", Compteur3.GetName());
            Assert.AreEqual(11, Compteur3.GetNum());
        }

        public void TestRemise_a_0()
        {
            Counter Compteur4 = new Counter("Compteur4", 14);
            
            Assert.AreEqual("Compteur1", Compteur4.GetName());
            Assert.AreEqual(0, Compteur4.GetNum());
        }

    }
}
