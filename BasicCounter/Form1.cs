﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CounterLib;

namespace BasicCounter
{
    public partial class Form1 : Form
    {
        Counter premier;
        public Form1()
        {
            InitializeComponent();
            this.premier = new Counter("premier", 0);
        }

        private void Moins_Click(object sender, EventArgs e)
        {
            label2.Text = String.Format(premier.decrementer().ToString());
        }

        private void Effacer_Click(object sender, EventArgs e)
        {
            label2.Text = String.Format(premier.remise_a_0().ToString());
        }

        private void Plus_Click(object sender, EventArgs e)
        {
            label2.Text = String.Format(premier.incrementer().ToString());
        }
    }
}
