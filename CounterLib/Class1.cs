﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterLib
{
    public class Counter
    {
        private string name;
        private int numero;

        public Counter(string name, int numero)
        {
            this.name = name;
            this.numero = numero;
        }
        public string GetName()
        {
            return this.name;
        }
        
        public int GetNum()
        {
            return this.numero;
        }
        public int decrementer()
        {
            return this.numero --;
        }

        public int incrementer()
        {
            return this.numero ++;
        }

        public int remise_a_0()
        {
            return this.numero = 0;
        }
        
    }
}
